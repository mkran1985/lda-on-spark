Readme File
Author: Muthukumaran Chandrasekaran
CSCI 6900: Mining Massive Datasets
Assignment 4 - LDA on Spark 
Date: 4/10/2015
-----------------------------------

Specs:
-------
Programming Language: Scala
OS: Windows (laptop)
IDE: IntelliJ Idea
Scala Version: 2.10.5
Spark Version: 1.3.0
Jar: lda-build.jar (Path: lda\out\artifacts\lda_build_jar)
Arguments: <Path\to\source\file> <Path\to\stop\file>

Command:
---------
I used the IDE to run my code. 
But I think if you run the following from command line it will work:

spark-submit lda-build.jar <Path\to\source\file> <Path\to\stop\file>

Logs File: 
logs\out-logsFinal.txt

What's not done
------------------
All the Bonus Questions. 

References:
------------
Baha talked to me for about 5 min about his general methodology
Ankita helped me with syntax here and there. 
Other references are mentioned in the comments.

Deliverables:
--------------
Project files are committed/pushed to https://bitbucket.org/mkran1985/assignment4

Question 1:
-----------
Couldn't get this to work on entire data set. So, ran it for the first 10000 tweets (Ankita gave me this concatenated list)
Total Unique Words: 27701

Question 2:
-----------
k=10, no. of cores = * (i.e., 4)
Listed out 10 topics and the distribution over words (commented out). 
Am able to extract the top 1 word (or something!) from this distribution. But don't know if this is correct. 

Question 3:
-----------
I collected a Stop Word List - stopwords.txt
#Unique words after filtering stop words: 27317

Question 4:
-----------
Attempted to use the NLP Tool Box from stanford to remove stop words. Did not work.
So attempted using filter. Worked!
I then reused the existing LDA code to re-run after stopwords were filtered out.
Getting an empty set of words :( 
Don't have time to debug.

Question 5:
--------------
Split each tweet into tokens, and for each token, find the topic that has the highest probability for that token, then we can say the tweet is of the topic that has the most tokens of the tweet.


Bonus Questions
-------------------
Skipped, Sorry!